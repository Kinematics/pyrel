This directory is for code associated with creating the map of the game. 
Somewhat confusingly, in addition to level generation, it also holds the
gameMap.py and cell.py modules, which deal with tracking and organizing all 
game objects, including after the map has been generated.

