import types

import commands
import container
import gui



## Encapsulates the common data structures and routines used in the main
# frame across UI implementations to handle communications between user
# command execution and their respective prompts
class CommandHandler(object):
    def __init__(self):
        super(CommandHandler, self).__init__()
        ## Current Command we're contextualizing.
        self.curCommand = None
        ## Current Command execution context
        self.commandCoroutine = None
        ## Current active prompt.
        self.curPrompt = None
        ## Last Command we executed.
        self.lastCommand = None


    ##Implemented by subclass to communicate prompt cancellation
    def doesKeyCancelPrompt(self, keyEvent):
        raise NotImplementedError('%s is missing doesKeyCancelPrompt method required by super class' % self)


    def receiveKeyInput(self, keyEvent):
        if self.curPrompt is not None:
            # Interpret the input based on the prompt, instead of sending
            # a command to the game.
            if self.doesKeyCancelPrompt(keyEvent):
                # Run exit routines if any
                self.curPrompt.onCancel()
                # Cancel the current prompt and command.
                self.curPrompt = None
                self.curCommand = None
                self.commandCoroutine = None
            else:
                promptResult = self.curPrompt.receiveKeyInput(
                        keyEvent, self.gameMap)
                self.curPrompt = promptResult.nextPrompt
                # \todo maybe end command if promprtResult.result is None
                if promptResult.result is not None:
                    try:
                        result = self.commandCoroutine.send(promptResult.result)
                        # Commands can chain into other commands.
                        if isinstance(result, commands.Command):
                            # End the current command, then start a new one.
                            self.endCommand()
                            self.curCommand = result
                            self.asyncExecute()
                    except StopIteration as e:
                        self.endCommand()
        elif self.curCommand:
            # This should never happen -- a Command being extant while
            # no prompts exist.
            raise RuntimeError("Got input while there's a Command but no Prompt.")
        else:
            # Start creating a Command.
            input = gui.keymap.convertKeyToCommand(keyEvent)
            if input is None:
                # Not a valid input; ignore it.
                return
            # \todo At this point, we're assuming that the player is the only
            # one performing Commands.
            player = self.gameMap.getContainer(container.PLAYERS)[0]
            self.curCommand = commands.inputToCommandClassMap[input](
                    player, input, self.gameMap )
            self.asyncExecute()


    ## Process current command
    def asyncExecute(self):
        coroutine = self.curCommand.contextualizeAndExecute()
        # Check if command yielded execution. If so, the command
        # is wrapped in a a generator object.  Invoke next() to run the command
        # up to its first yield statement.  There's probably a better place to
        # save this coroutine object, but it needs to be accessed by
        # gui.prompt.resolvePrompt so that new values can be sent to it.
        if isinstance(coroutine, types.GeneratorType):
            self.commandCoroutine = coroutine
            try:
                result = self.commandCoroutine.next()
                # Commands can chain into other Commands, or they can yield
                # Prompts.
                if isinstance(result, commands.Command):
                    self.endCommand()
                    self.curCommand = result
                    self.asyncExecute()
            except StopIteration as e:
                # When a coroutine reaches the end of execution
                # it raises a StopIteration exception, i.e., the
                # command has finished executing.
                self.endCommand()
        else:
            # Command ended immediately without needing a generator
            self.endCommand()


    ## Handle a request for a new Prompt.
    def receivePrompt(self, prompt):
        self.curPrompt = prompt


    ## Terminate our current Command; remember it for possible later
    # repetition, then update the game state.
    def endCommand(self):
        self.lastCommand = self.curCommand
        self.curCommand = None
        self.commandCoroutine = None
        self.curPrompt = None

