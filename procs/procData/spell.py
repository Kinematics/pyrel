import procs.procLoader
import util.record

import collections
import os



## Spell data describes a single spell that can be cast. Definitions are 
# loaded from spell.txt. They are functionally Proc records with some 
# extra required fields.
class Spell:
    def __init__(self, record):
        ## Keep a copy around in case we need to make a new Spell based on us.
        self.record = record
        ## Name of the Spell.
        self.name = record['name']
        ## Message to display when the Spell is cast.
        self.message = record.get('message', None)
        ## Proc code to invoke for the spell.
        self.proc = procs.procLoader.generateProcFromRecord(record)
        ## Targeting for the spell.
        self.target = record.get('target', 'self')


    ## Passthrough to our Proc.
    def trigger(self, *args, **kwargs):
        self.proc.trigger(*args, **kwargs)



## Maps element names to Element instances.
NAME_TO_SPELL = collections.OrderedDict()

def loadFiles():
    for spell in util.record.loadRecords(
            os.path.join('data', 'spell.txt'), Spell):
        NAME_TO_SPELL[spell.name] = spell


## Generate a Spell instance. The "record" parameter may just be a string,
# in which case we look up one of our pre-loaded spells; otherwise we generate
# a new one (potentially using a pre-loaded spell as a template).
def getSpell(record):
    if isinstance(record, str) or isinstance(record, unicode):
        if record not in NAME_TO_SPELL:
            raise NameError("Tried to retrieve nonexistent spell %s" % record)
        return NAME_TO_SPELL[record]
    # We need to create a new spell.
    if 'name' in record:
        baseSpell = getSpell(record['name'])
        newRecord = dict(baseSpell.record)
        newRecord.update(record)
        return Spell(newRecord)

 
