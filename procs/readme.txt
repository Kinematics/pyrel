Procs are bits of code that modify the game world or control the flow of 
execution. Perhaps more usefully, procs are where all of the little details
of the game exist ("What happens when I cast fireball?" "How do we keep unique
monsters from being generated after they've been killed?" "How does the AI
decide what to do on its turn?"). 

For more information on procs, please see 
https://bitbucket.org/derakon/pyrel/wiki/ProcsGuide
