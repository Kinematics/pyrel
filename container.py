
## Unique, decrementing global ID for Containers that lack a more specific ID.
uniqueID = -1

## IDs for "fundamental" containers that will recur elsewhere in the code.
(
## Must implement ascend(self, actor, gameMap)
ASCENDABLES,
## Must implement receiveAttack(self, alt)
ATTACKERS,
## Potentially obstructs movement. Must implement canMoveThrough(self, alt)
BLOCKERS,
## Must implement pickupItem(self, item), removeItem(self, item)
CARRIERS,
## Must implement close(self, actor, gameMap)
CLOSABLES,
## No required functions.
CREATURES,
## Must implement descend(self, actor, gameMap)
DESCENDABLES,
## No reqiured functions. Marks entities that the view should jump to when
# looking around.
INTERESTING,
## Must implement pickup(self), drop(self), getShortDescription(self)
ITEMS,
## Obstructs vision. No required functions.
OPAQUES,
## Must implement open(self, actor, gameMap)
OPENABLES,
## Marks the Thing as persisting across dungeon levels. Must implement
# resubscribe(self, gameMap).
PERSISTENT,
## We just like to keep track of this guy.
PLAYERS,
## No required functions.
TERRAIN,
## Must implement tunnel(self, actor, gameMap)
TUNNELABLES,
## Must implement update(self). Must have getStat('speed'), as well as
# "energy" and "name" fields. Recommend using the things.mixins.updater mixin.
UPDATERS,
## Must implement invoke(self).
USABLES,
## No required functions, but must have an equipSlot field.
WIELDABLES,
) = range(18)


## This class is basically a wrapper around the builtin set class, though it's
# very slightly smarter (you can set a callback to get notified when it empties,
# and we may add more later).
class Container:
    ## \param key Identifier for the Container. Uses a unique
    #         (auto-decrementing) numeric key by default.
    # \param sortFunc Function to use for sorting our items when iterating over
    #        them.
    # \param members Things that start out in the container.
    # \param notifyOnEmpty Function to call if we're ever empty.
    def __init__(self, key = None, members = None, sortFunc = None,
            notifyOnEmpty = None):
        self.key = key
        if key is None:
            global uniqueID
            self.key = uniqueID
            uniqueID -= 1
        self.members = members
        if self.members is None:
            self.members = set()
        self.sortFunc = sortFunc
        self.notifyOnEmpty = notifyOnEmpty


    ## Receive a new member.
    def subscribe(self, member):
        self.members.add(member)


    ## Remove a member.
    def unsubscribe(self, member):
        self.members.remove(member)
        if not self.members and self.notifyOnEmpty is not None:
            self.notifyOnEmpty()


    ## Get members that intersect the given Container.
    def getIntersection(self, alt):
        return Container(members = self.members.intersection(alt.members),
                sortFunc = self.sortFunc)

    ## Get members that are disjoint to the given Container
    def getDifference(self, alt):
        return Container(members = self.members.difference(alt.members),
                sortFunc = self.sortFunc)


    ## Add alt's Things to our set.
    def unionAdd(self, alt):
        self.members.update(alt.members)


    ## Empty ourselves out entirely.
    def setEmpty(self):
        self.members.clear()


    ## Iterate over our contents.
    def __iter__(self):
        iterable = self.members
        if self.sortFunc is not None:
            iterable = sorted(list(self.members), self.sortFunc)
        for item in iterable:
            yield item


    ## Get the number of items we have.
    def __len__(self):
        return len(self.members)


    ## Retrieve the indicated item from our set. This only works reliably
    # if we have a sorting function since sets are otherwise inherently
    # unordered.
    def __getitem__(self, index):
        iterable = list(self.members)
        if self.sortFunc is not None:
            iterable.sort(self.sortFunc)
        return iterable[index]


    ## Return true if we contain the specified item.
    def __contains__(self, item):
        return item in self.members


    ## Cast to a boolean -- return True if we have any subscribers.
    def __nonzero__(self):
        return bool(self.members)


    ## For debugging use, represent as a string.
    def __repr__(self):
        return "<Container %s with contents: %s>" % (self.key, self.members)



## As Container is a smart wrapper around the builtin set, ContainerMap is a
# smart wrapper around the builtin dict. Sometimes we need a mapping
# relationship that can still be conveniently indexed into.
class ContainerMap(dict):
    ## \param key Identifier for the ContainerMap. Uses a unique
    #         (auto-decrementing) numeric key by default.
    # \param sortFunc Function to use for sorting our items when iterating over
    #        them or indexing into them.
    # \param members Initial dict of stuff starting out in the container.
    # \param notifyOnEmpty Function to call if we're ever empty.
    def __init__(self, key = None, members = None, sortFunc = None,
            notifyOnEmpty = None):
        dict.__init__(self)
        self.key = key
        if key is None:
            global uniqueID
            self.key = uniqueID
            uniqueID -= 1
        if members is not None:
            self.update(members)
        self.sortFunc = sortFunc
        self.notifyOnEmpty = notifyOnEmpty


    ## Receive a new key-value mapping.
    def subscribe(self, key, member):
        self[key] = member


    ## Remove whatever is under the given key.
    def unsubscribe(self, key):
        member = self[key]
        del self[key]
        if not self and self.notifyOnEmpty is not None:
            self.notifyOnEmpty()


    ## Iterate over our contents, using our sort function if relevant.
    def __iter__(self):
        iterable = self.keys()
        if self.sortFunc is not None:
            iterable.sort(self.sortFunc)
        for item in iterable:
            yield item


    ## Override so we operate in sorted order.
    def items(self):
        return [i for i in self.iteritems()]


    ## Override so we operate in sorted order.
    def iteritems(self):
        for key in self:
            yield (key, self[key])


    ## Override so we operate in sorted order.
    def values(self):
        return [self[key] for key in self]


    ## Retrieve the indicated item from our dict. This only works reliably
    # if we have a sorting function since sets are otherwise inherently
    # unordered.
    def getByIndex(self, index):
        iterable = self.keys()
        if self.sortFunc is not None:
            iterable.sort(self.sortFunc)
        return self[iterable[index]]


    ## Access an item in our dict via its key. We put a hack in here to
    # try self.getByIndex() if the key is an int and isn't already in the
    # dict.
    def __getitem__(self, key):
        if key in self:
            return dict.__getitem__(self, key)
        if type(key) is int and key < len(self):
            return self.getByIndex(key)


    ## Hashing function; we do lookup by our unique ID.
    def __hash__(self):
        return hash(self.key)


    ## For debugging use, represent as a string.
    def __repr__(self):
        return "<ContainerMap %s with %d items>" % (self.key, len(self))

