import json
import types

import container

## The Serializer and Deserializer classes are used to [de]serialize game state
# by converting it to/from JSON. 
# To qualify for inclusion by these classes, an object must either be a 
# Container (or ContainerMap), or:
# - have the functions getSerializationDict() and getContainers(). The former
#   returns a dict that can be passed to json.dumps(); the latter returns an
#   ordered list of Containers that the Thing cares about. 
# - be registered in our mapping of object types to deserialization functions.
# - have an 'id' field that is unique across all objects and can be manually
#   set when deserializing (thus, cannot be the Python object ID produced by 
#   the builtin function id()). 
#
# To use the Serializer, create it, add whatever containers and things you
# want to it, and then call its writeFile() function. 
# 
# Note: one of the primary goals of this system is to avoid having any actual
# code in the savefile. Thus why we don't use pickle or other existing 
# automatic serialization systems -- they can have just about anything in them,
# so you have to trust your inputs to not be malicious. 
# 
# On a similar note, if we ever added a database to Pyrel, we would need to
# be certain not to include database queries in the savefile...



## This class handles creating savefiles. Usage:
# serializer = Serializer()
# serializer.addContainer(...) # recursively adds containers and contents
# serializer.addThing(...) # adds thing and anything it's carrying
# serializer.writeFile(filename)
class Serializer:
    def __init__(self):
        ## Maps Container IDs to the IDs of the contents of those containers.
        self.idToContainerContents = {}
        ## Maps Thing IDs to serializations of those Things.
        self.idToThingSerialization = {}
        ## Maps objects to their Pyrel-generated IDs.
        self.objectToId = {}


    ## Add the specified container and its contents to our serialization.
    def addContainer(self, inputContainer):
        if inputContainer.key in self.idToContainerContents:
            # We've already processed this inputContainer.
            return
        if isinstance(inputContainer, container.Container):
            self.addContainerSet(inputContainer)
        elif isinstance(inputContainer, container.ContainerMap):
            self.addContainerMap(inputContainer)
        else:
            raise TypeError("Unrecognized container type %s" % inputContainer)
        self.objectToId[inputContainer] = inputContainer.key


    ## Add a basic Container (derived from the builtin set() type, hence the 
    # function name). 
    def addContainerSet(self, inputContainer):
        self.idToContainerContents[inputContainer.key] = []
        for thing in inputContainer.members:
            self.idToContainerContents[inputContainer.key].append(thing.id)
            self.addThing(thing)


    ## Add a ContainerMap. 
    def addContainerMap(self, inputContainer):
        self.idToContainerContents[inputContainer.key] = {}
        for key, thing in inputContainer.iteritems():
            self.idToContainerContents[inputContainer.key][key] = thing.id
            self.addThing(thing)


    ## Add the specified Thing to our serialization, as well as any containers
    # it contains.
    def addThing(self, thing):
        if thing.id in self.idToThingSerialization:
            # We've already processed this thing.
            return
        self.idToThingSerialization[thing.id] = thing.getSerializationDict()
        self.objectToId[thing] = thing.id
        for container in thing.getContainers():
            self.addContainer(container)


    ## Write out our serialization to the given filename.
    def writeFile(self, filename):
        handle = open(filename, 'w')
        handle.write('{\n')
        
        # Start by writing out all of our containers' contents.
        idToContents = {}
        for id, contents in self.idToContainerContents.iteritems():
            idToContents[id] = contents
        output = json.dumps(idToContents, sort_keys = True, indent = 2)
        handle.write('"containers": %s,\n\n' % output)

        idToThing = {}
        # Now write the Thing serializations. We have to "clean" them
        # (replace function pointers with something more amenable to 
        # serialization) in the process.
        for id, serialization in self.idToThingSerialization.iteritems():
            serialization = self.cleanDict(serialization)
            idToThing[id] = serialization
        output = json.dumps(idToThing, sort_keys = True, indent = 2)
        handle.write('"things: %s,' % output)
        
        handle.write('}\n')
        handle.close()


    ## Given a dict, ensure that it's ready for serialization. We replace
    # function pointers with strings (see makeFunctionString() below) and 
    # ensure that everything else is of a valid type. We also recurse through
    # lists and dicts.
    def cleanDict(self, inputDict):
        result = {}
        for key, value in inputDict.iteritems():
            newKey = self.cleanValue(key)
            newValue = self.cleanValue(value)
            result[newKey] = newValue
        return result


    ## Given a value, recursively "clean" it per the commentary for cleanDict.
    def cleanValue(self, value):
        if isinstance(value, types.MethodType):
            # Value is a function.
            return self.makeFunctionString(value)
        elif isinstance(value, dict):
            # Recurse
            return self.cleanDict(value)
        elif isinstance(value, list):
            # Check each value in turn.
            return [self.cleanValue(v) for v in value]
        else:
            # Check for any other types we can serialize; reject everything
            # else. 
            for validType in [types.StringTypes, types.BooleanType, 
                types.FloatType, types.IntType, types.LongType, types.NoneType]:
                if isinstance(value, validType):
                    return value
            # Unrecognized type.
            raise RuntimeError("Attempted to serialize unrecognized object type %s" % type(value))


    ## Given an input object that is a function, generate a string of the 
    # form
    # "__pyrelFunctionPointer__:object ID:function name".
    def makeFunctionString(self, func):
        # The im_self field on functions contains the object the function
        # is bound to. 
        obj = func.im_self
        if not hasattr(obj, 'id'):
            # We can only serialize functions if they are bound to objects
            # that we are also serializing. 
            raise RuntimeError("Tried to serialize a function %s bound to an object with no ID: %s" % (str(func), str(obj)))
        boundId = obj.id
        if obj not in self.objectToId:
            # Unrecognized object ID. 
            raise RuntimeError("Tried to serialize a function %s bound to object with unknown ID %s" % (str(func), boundId))
        # The __name__ field is the function's name in string form.
        funcName = func.__name__
        return "__pyrelFunctionPointer__:%s:%s" % (boundId, funcName)


