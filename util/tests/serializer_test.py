import container
import util.serializer



globalId = 0


## Simple Thing for testing serializations.
class DummyThing:
    def __init__(self):
        global globalId
        self.id = globalId
        globalId += 1
        self.funcPointer = None


    def testA(self):
        print "Hello, world!"


    def testB(self):
        print "Goodbye, world!"


    def getSerializationDict(self):
        return {'id': self.id, 'funcPointerAsVal': self.funcPointer, 
                self.funcPointer: 'funcPointerAsKey'}


    def getContainers(self):
        return []



def test1():

    thing1 = DummyThing()
    thing2 = DummyThing()
    thing1.funcPointer = thing2.testA
    thing2.funcPointer = thing1.testB

    holder = container.Container(key = 'test1Container')
    holder.subscribe(thing1)
    holder.subscribe(thing2)

    serializer = util.serializer.Serializer()
    serializer.addContainer(holder)
    serializer.writeFile('test.txt')
    print "Test 1 complete"


test1()

