import container
import things.thing
import things.mixins.updater

## Auto-incrementing ID used to generate marker names.
globalId = 0


## A Marker just marks a location on the map. It has a location and an age;
# that's it.
class Marker(things.thing.Thing, things.mixins.updater.Updateable):
    def __init__(self, pos, gameMap, name = None, speed = 1):
        things.thing.Thing.__init__(self, name = name)
        things.mixins.updater.Updateable.__init__(self, gameMap, 
                name = name, speed = speed, energy = 0)
        self.pos = pos
        self.gameMap = gameMap
        self.age = 0


    def update(self, *args):
        self.age += 1
        self.energy = 0


    def __repr__(self):
        return "<Marker at %s with age %d>" % (str(self.pos), self.age)
