import container
import things.thing
import things.mixins.updater

## Auto-incrementing ID used to generate timer names.
globalId = 0

## A Timer is an entity that waits a certain number of normal-speed turns, 
# calls a function, and then dies.
class Timer(things.thing.Thing, things.mixins.updater.Updateable):
    ## \param duration How many turns to wait.
    # \param func Function to call when our time runs out.
    # \param speed Rate at which turns pass 
    #        (1 = normal speed, 2 = double, etc.).
    def __init__(self, gameMap, duration, func, name = None, speed = 1):
        things.thing.Thing.__init__(self, name = name)
        things.mixins.updater.Updateable.__init__(self, gameMap, 
                name = name, speed = speed, energy = 0)
        self.gameMap = gameMap
        self.duration = duration
        self.func = func


    def update(self, *args):
        self.duration -= 1
        self.energy = 0
        if self.duration <= 0:
            self.func()
            self.gameMap.destroy(self)


    ## Change the duration to the specified value.
    def setDuration(self, newDuration):
        self.duration = newDuration

